from random import choices
from django.db import models
from doctor.models import Doctor
# Create your models here.

class Patient(models.Model):
    full_name = models.CharField(max_length=50)
    phone = models.IntegerField()
    email = models.EmailField(max_length=254)
    age = models.IntegerField()
    

    def __str__(self):
        return self.full_name



class appointement(models.Model):
    
    choices = (
        ("waiting","waiting"),
        ("completed","completed")
    )

    doctor = models.ForeignKey(Doctor,related_name="appointement", on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient,related_name="appointement", on_delete=models.CASCADE)
    number = models.IntegerField(default=0)
    date = models.DateField(auto_now=False, auto_now_add=False)
    overview = models.TextField(blank=True)
    status = models.CharField(max_length=50,choices=choices,default="waiting")

    

    def __str__(self):
        return  f"{self.number}-{self.patient.full_name}-{self.date}-{self.doctor.full_name}"




