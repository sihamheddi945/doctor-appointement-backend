
# Create your views here.
from datetime import datetime
from rest_framework.decorators import api_view
# Create your views here.
from django.db.models import Q
from rest_framework.response import Response
from doctor.models import Doctor
from appointement.models import appointement , Patient
from appointement.serializer import PatientSerializer , AppoSerializer , AppointementSerializer
from django.core.paginator import Paginator

@api_view(["POST"])
def delete_appointement(request):
    try:
        
        id = request.data.get("id")
        appo = appointement.objects.get(pk=id)
        appo.delete()

        return Response({"message":"the appointement was successfully deleted"},status=200)
    except appointement.DoesNotExist:
        return Response({"message":"this appointement does not exist"},status=404)




@api_view(["GET"])
def appos_for_doctor(request):
    try:
        id = request.GET.get("id")
        doctor = Doctor.objects.get(pk=id)

        today = datetime.today()
        appo = doctor.appointement.filter(date=today,status="waiting").all()
        num_pages = 0
        if len(appo) > 4:
                paginator = Paginator(appo,4) # Show 4 appointement per page.
                page_number = request.GET.get('page')
                num_pages = paginator.num_pages
                appo = paginator.get_page(page_number)

        data = AppoSerializer(appo,many=True).data
        d = {
            "data":data,
            "num_pages":num_pages
        }

        return Response(d ,status=200)
    except Doctor.DoesNotExist:
        return Response({
            "message": "There is no doctor with that id"
        },status=400)






















@api_view(["POST"])

def appo_create(request):    
    doctor_id = request.data.get("did")
    try:

        doctor = Doctor.objects.get(pk=doctor_id)



        email = request.data.get("email")
        phone = request.data.get("phone")
        full_name = request.data.get("full_name")
  
        patient = PatientSerializer(data=request.data)

        if patient.is_valid():
            try:
                patient1 = Patient.objects.filter(Q(full_name__iexact=full_name,email=email,phone=phone)).get() 
                try:

                    p = patient1.appointement.filter(date=request.data.get("date") ,doctor__id=doctor_id).get()
                    # print(str(p.date) == request.data.get("date"))
                
                    if p:
                        return Response({
                            "message":"your appointement is already exist in date "+ str(request.data.get("date"))
                        },status=400)   
                except appointement.DoesNotExist:
                    pass                 

            except Patient.DoesNotExist:
                patient1=patient.save()
          
            try:
                appo = doctor.appointement.filter(date=request.data.get("date")).all()
                print(appo)
                if len(appo) == doctor.number_patient_per_day:
                    return Response({
                        "message":"You can't reserve appointement because you exceed the max number of patient of this doctor for this day . Please change it to another day",

                    },status=400)

            except appointement.DoesNotExist:
              pass
            a = AppointementSerializer(data=request.data)
            if a.is_valid():
                n = appointement.objects.filter(date=request.data.get("date"),doctor_id=doctor_id).last()
                number =  n.number+1 if n  else 1
                a.save(patient=patient1,doctor=doctor,number=number,overview=request.data.get("overview"))
                date = datetime.strptime(request.data.get("date"), '%Y-%m-%d')
                print(date)
                day = date.strftime('%A').lower()
                print(day)
                hour_start = doctor.hour_start_for_day(day).hour
                print(hour_start)
                return Response({
                    "message":f"the appointement was succefully created . Your number is {number} . Maybe your appointement will be at {int(hour_start)+(number-1)}h"
                },status=200)
            else:
                return Response({
                "error":"The information of appointement is not valid"
            },status=400)

        else:
            return Response({
                "error":"The information of patient is not valid"
            },status=400)

    except Doctor.DoesNotExist:
        return Response({
            "error":"the doctor with this id doesn't exist"
        },status=404)


