

from rest_framework import serializers
from .models import Patient ,appointement

class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = "__all__"



class AppoSerializer(serializers.ModelSerializer):

    patient = PatientSerializer()
    class Meta:
        model = appointement
        fields = ["id","patient","overview","status"]





class AppointementSerializer(serializers.ModelSerializer):
    class Meta:
        model = appointement
        exclude=["number","doctor","patient","overview"]