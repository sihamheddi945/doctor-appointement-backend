from django.urls import path 
from . import views


urlpatterns = [

    path("create/",views.appo_create,name="appo.create"),
    path("doctor/",views.appos_for_doctor,name="appo.doctor"),
    path("delete/",views.delete_appointement,name="appo.delete")

]