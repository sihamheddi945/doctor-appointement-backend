from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from .models import Conclusion


class ConclusionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conclusion
        fields = "__all__"

 
