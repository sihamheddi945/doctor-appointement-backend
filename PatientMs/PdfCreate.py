from fpdf import FPDF, HTMLMixin
from appointement.models import appointement
from django.conf import settings


class PDF(FPDF, HTMLMixin):
    pass


def prescription_create(id,content):
    try:
        appo = appointement.objects.get(pk=id)
        doctor = appo.doctor
        patient = appo.patient
        
        data = (
            ("", "",""),
            (f"Date : {appo.date}", f"Full Name : {patient.full_name}",f"Age :  {patient.age}")
        )

        pdf = PDF()
        pdf.add_page()
        pdf.set_font('Arial', 'B', 14)
        # Move to 8 cm to the right
        pdf.cell(80);
        pdf.set_text_color(26, 178, 255)
        # Centered text in a framed 20*10 mm cell and line break
        pdf.cell(20,10,"People's Democratic Republic of Algeria",0,1,'C');
        pdf.set_font('Arial', '', 12)
        pdf.cell(190,10,f"Dr.{doctor.full_name} {doctor.specialization}",0,0.5,'C');
        pdf.cell(190,10,f"{doctor.adress} {doctor.state}",0,0.5,'C');
        pdf.cell(190,10,f"Email : {doctor.email} , Phone :  {doctor.phone} ",0,2,'C');

        pdf.write_html(f"""<table>
            <thead>
            <tr>
            
                <th width="33%">{data[0][0]}</th>
                <th width="33%">{data[0][1]}</th>    
                <th width="33%">{data[0][2]}</th>
            
            </tr>  
            </thead>
            <tbody>
                <tr > 
                    <td>{'</td><td>'.join(data[1])} </td>

                </tr>
            </tbody>
        </table>
        """)
        pdf.cell(80,10,"",0,1);
        pdf.set_font('Arial', 'B', 16)
        pdf.set_text_color(26, 178, 255)
        pdf.cell(190,10,"Prescription",1,2,'C');
        
        pdf.set_font('Arial', '', 16)
        
        pdf.set_text_color(38, 37, 37)
        pdf.cell(80,10,"",0,1);
        pdf.multi_cell(190,10,content,0,1);
        # pdf.cell(20,10,"Dr.Bla bla Specialization ")
        # pdf.cell(20,0,"adress , State",ln=1)
        # pdf.cell(20,10,"adress , State",ln=1)
        # pdf.cell(20,10,"Tel : 053443433 , Email : fafa@contact.co",ln=1)
        url = f'media/prescription/{id}.pdf'
        url_host = f"{settings.HOST}/media/prescription/{id}.pdf" 
        pdf.output(url,"F")
        return url_host
    except appointement.DoesNotExist:
        return None

prescription_create(40,"content")