from django.apps import AppConfig


class PatientmsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PatientMs'
