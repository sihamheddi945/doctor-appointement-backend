from django.db import models

from appointement.models import appointement


class Conclusion(models.Model):
    
    description = models.TextField()
    
   
    appointement = models.ForeignKey(appointement, related_name="conclusion", on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.appointement.patient.full_name} - {self.appointement.date}'

class prescription(models.Model):

    content = models.TextField()
    conclusion = models.ForeignKey(Conclusion,related_name="prescription", on_delete=models.CASCADE)
    pdf_url = models.URLField(max_length=200)

