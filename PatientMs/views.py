
from rest_framework.decorators import api_view
from .serializer import ConclusionSerializer
from appointement.models import appointement
from .PdfCreate import prescription_create
from rest_framework.response import Response
from .models import prescription








@api_view(['POST'])
def conclusion_create(request):
    try:
        id = request.data.get("appointement")
        ans = request.data.get("ans")
        a = appointement.objects.get(pk=id)
        serializer = ConclusionSerializer(data=request.data)
        url=""
        if serializer.is_valid():
            c = serializer.save()
            if ans:
                content = request.data.get("content")
               
                url=prescription_create(id,content) or ''
                
            
                prescription.objects.create(content=content,conclusion=c,pdf_url=url)
            a.status = "completed"
            a.save()
            return Response({"message":"The conclusion was successfully created !","url":url},status=200)
        else:
            return Response({"message":"The data is not valid"})
    except appointement.DoesNotExist:
        return Response({"message":"there is no appointement with that id"},status=404)
       
