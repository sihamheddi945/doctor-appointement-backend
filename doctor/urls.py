

from . import views
from django.urls import path 


urlpatterns = [
    path("search/",views.search_doctors,name="doctors.search"),
    path("complete/",views.profile_doctor_complete, name="doctor.complete_profile"),
    path("",views.get_doctor,name="doctor.get")
]
