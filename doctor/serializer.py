


from rest_framework import serializers

from doctor.models import Doctor , WorkingHours


class WorkingHour(serializers.ModelSerializer):
    class Meta:
        model = WorkingHours
        fields = ["id","day","hour_start","hour_end"]

class DoctorSerialzier(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = ["id","full_name","email","get_url","phone","working_hours_today","adress","state","distance", "specialization","image","description"]

class DoctorSerialzier1(serializers.ModelSerializer):
    hours = WorkingHour(many=True,read_only=True)
    class Meta:
        model = Doctor
        fields = ["id","full_name","email","get_url","phone","hours","adress","state","distance", "specialization","description"]
        

