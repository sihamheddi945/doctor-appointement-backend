from django.db import models
from django.contrib.auth.models import AbstractBaseUser , BaseUserManager
from django.conf import settings
from django.forms.models import model_to_dict

from datetime import datetime

class UserManager(BaseUserManager):

    def create_superuser(self,email,password,full_name, phone,adress,distance,state,specialization, **extra_fields):

        
        user = self.create_user(email , password, full_name, phone,adress,distance,state,specialization, **extra_fields)
        
        user.is_staff = True
        user.is_active = True
        user.is_admin = True
        user.is_superuser = True
        user.save()
        return user

    def create_user(self,email , password, full_name, phone,adress,distance,state,specialization, **extra_fields):
        if not email:
            ValueError("the email is required")
        
        if not full_name:
            ValueError("the full name is required")
        if not phone:
            ValueError("the phone is required")
        if not adress:
            ValueError("the adress is required")
        if not distance:
            ValueError("the distance is required")
        if not state:
            ValueError("the state is required")
        if not specialization:
            ValueError("the specialization is required")

        email = self.normalize_email(email)
        user = self.model(email=email, full_name=full_name, phone=phone,adress=adress,distance=distance,state=state,specialization=specialization, **extra_fields)
        
        user.set_password(password)
        user.save()
        return user


# Create your models here.
def image_upload(instance,filename):
   
    extension = filename.split(".")[1]
    return f"profiles/{instance.id}.{extension}"

def certf_upload(instance,filename):
    extension = filename.split(".")[1]
    return f"certafication/{instance.id}.{extension}"








class Doctor(AbstractBaseUser):

    choices = (
        ("Cardiologist and chest diseases","Cardiologist and chest diseases"),
        ("Orthopedic and joint doctor","Orthopedic and joint doctor"),
        ("Optometrist","Optometrist"),
        ("radiologist","radiologist"),
        ("Gynecologist","Gynecologist"),
        ("Pediatrician","Pediatrician"),
        ("Ear,Nose and Throat","Ear,Nose and Throat"),
        ("generalist","generalist"),
        ("dentist","dentist"),
        ("surgeon","surgeon"),
        ("Cancer and infectious diseases doctor","Cancer and infectious diseases doctor"),
        ("skin doctor","skin doctor"),
        ("Diabetes and blood pressure doctor","Diabetes and blood pressure doctor")

    )


    full_name = models.CharField(max_length=250)
    email = models.EmailField(max_length=254,unique=True)
    phone = models.IntegerField()
    state = models.CharField(max_length=254)
    adress = models.CharField(max_length=254)
    distance = models.FloatField()
    specialization = models.CharField(max_length=254,choices=choices)
    image = models.ImageField(upload_to=image_upload, default="profiles/default.png")
    certificate_url = models.FileField(upload_to=certf_upload, max_length=100,null=True,blank=True)
    password = models.CharField(max_length=266)
    number_patient_per_day = models.IntegerField(default=10)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    description = models.TextField()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["full_name","phone","state","adress","distance","specialization","is_complete"] 
    objects = UserManager()

    def get_url(self):
        return settings.HOST+self.image.url

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    def working_hours_today(self):
        today = datetime.today()
        day = today.strftime('%A').lower()
        for e in self.hours.all():
            if e.day == day:
                return model_to_dict(e)
        return None 

    def hour_start_for_day(self,day):

        for e in self.hours.all():
            if e.day == day:
                return e.hour_start
     
    def __str__(self):
        return self.full_name




class WorkingHours(models.Model):
    choices = (
        ("sunday","sunday"),
        ("monday","monday"),
        ("tuesday","tuesday"),
        ("wednesday","wednesday"),
        ("thursday","thursday"),
        ("friday","friday"),
        ("saturday","saturday"),
    )

    day = models.CharField(max_length=50,choices=choices)
    hour_start = models.TimeField(auto_now=False, auto_now_add=False)
    hour_end  = models.TimeField(auto_now=False, auto_now_add=False)
    doctor = models.ForeignKey(Doctor,related_name="hours", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.day} - {self.doctor.full_name}"




















