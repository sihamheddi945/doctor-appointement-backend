from django.contrib import admin

from doctor.models import Doctor , WorkingHours


# Register your models here.
admin.site.register(Doctor)
admin.site.register(WorkingHours)
