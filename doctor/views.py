
from rest_framework.decorators import api_view
import json
from django.db.models import Q
from rest_framework.response import Response
from doctor.models import Doctor, WorkingHours
from doctor.serializer import DoctorSerialzier , DoctorSerialzier1
from django.core.paginator import Paginator


@api_view(['POST'])
def profile_doctor_complete(request):
    id = request.data.get("id")
    
    try:

        doctor = Doctor.objects.get(pk=id)
        wh = json.loads(request.data.get("wh"))
        # print(w[0].get("day"))
        for w in wh:

            day = w.get("day")
            hour_start = w.get("hour_start") 
            hour_end = w.get("hour_end")
            
            h = WorkingHours()
            h.day = day
            h.hour_start = hour_start
            h.hour_end = hour_end
            h.doctor = doctor
            h.save()
        
        doctor.description =  request.data.get("overview")
        doctor.certificate_url = request.data.get("certf")
        doctor.number_patient_per_day = request.data.get("nb")

        image =  request.data.get("image")

        print("- I'm image : ",image)
        print("- I'm certf : ",request.data.get("certf"))
        
        if image:
            doctor.image = image

        doctor.is_complete = True
        doctor.save()

        return Response({
            "message":"Your Profile is now completed",
            "success":True
        },status=200)

    except Doctor.DoesNotExist:
        return Response(data={
            "error":" the doctor with this id doesn't exist "
        },status=404) 

@api_view(['GET'])
def get_doctor(request):

    id1 = request.GET.get("id")
    try:
        doctor = Doctor.objects.get(id=id1)


        data = DoctorSerialzier1(doctor).data
        return Response(data,status=200)
    except Doctor.DoesNotExist:
        return Response(data={
            "error":" the doctor with this id doesn't exist "
        },status=404) 







@api_view(['GET'])
def search_doctors(request):
    
    state = request.GET.get("state")
    specialization = request.GET.get("spec")
    distance =  request.GET.get("distance",0)
    name = request.GET.get("name","")
    print(state,specialization,distance,name)
    if state and specialization:
        queryset = Q(state__iexact=state,distance__gte=distance, 
    full_name__icontains=name ,specialization__iexact=specialization)
    elif state:
        queryset = Q(state__iexact=state,distance__gte=distance, 
    full_name__icontains=name)
    elif specialization:
        queryset = Q(specialization__iexact=specialization, 
    full_name__icontains=name)
    else:
        queryset = Q(full_name__icontains=name)

    doctors = Doctor.objects.filter(queryset).all()
    num_pages = 0
    if len(doctors) > 5:
        paginator = Paginator(doctors,5) # Show 3 contacts per page.
        page_number = request.GET.get('page')
        num_pages = paginator.num_pages
        doctors = paginator.get_page(page_number)

    data = DoctorSerialzier(doctors,many=True).data
    r = {
        "doctors":data,
        "num_pages":num_pages
    }
    return Response(r,status=200)
